# What is I/O?

In computing, input/output or I/O (or, informally, io or IO) is the communication between an information processing system, such as a computer, and the outside world, possibly a human or another information processing system. Inputs are the signals or data received by the system and outputs are the signals or data sent from it. The term can also be used as part of an action; to "perform I/O" is to perform an input or output operation. I/O devices are used by a human (or other system) to communicate with a computer. For instance, a keyboard or computer mouse is an input device for a computer, while monitors and printers are output devices. Devices for communication between computers, such as modems and network cards, typically perform both input and output operations.

Note that the designation of a device as either input or output depends on perspective. Mouse and keyboards take physical movements that the human user outputs and convert them into input signals that a computer can understand; the output from these devices is the computer's input. Similarly, printers and monitors take signals that a computer outputs as input, and they convert these signals into a representation that human users can understand. From the human user's perspective, the process of reading or seeing these representations is receiving input; this type of interaction between computers and humans is studied in the field of human–computer interaction.

In computer architecture, the combination of the CPU and main memory, to which the CPU can read or write directly using individual instructions, is considered the brain of a computer. Any transfer of information to or from the CPU/memory combo, for example by reading data from a disk drive, is considered I/O. The CPU and its supporting circuitry may provide memory-mapped I/O that is used in low-level computer programming, such as in the implementation of device drivers, or may provide access to I/O channels. An I/O algorithm is one designed to exploit locality and perform efficiently when exchanging data with a secondary storage device, such as a disk drive.


# Differences between synchronous and blocking operations

Both these concepts (synchronous/asynchronous vs. blocking/non-blocking) are very similar but they do have a few clear differences which not many developers know about. In this article I will discuss how these concepts relate to each other.

### Synchronous operations

Understanding what synchronous means should be fairly easy for web developers because HTTP is a synchronous protocol. The web browser sends a request to the server and waits for its response. Then it sends the next request and again waits for its response, and so on. Having to wait for the response before sending the next request is often a pretty huge bottleneck. 

### Blocking operations

The concept of blocking is also easy to understand. Let's see the description of Java's InputStream.read method (used to read files):
 
```
If no byte is available because the end of the stream has been reached, the value -1 is returned. This method
blocks until input data is available, the end of the stream is detected, or an exception is thrown.
```
 
This call is called blocking because it will wait for the input data to become available. This too can create performance bottlenecks because the thread on which the call is made will stop from further execution. 

# Differences between asynchronous and non-blocking operations

### Asynchronous operations

Asynchronous and non-blocking I/O are the opposites of synchronous and blocking. In a sense both are fairly similar in that they allow your code to make an API call in a way that does not hold up the entire thread of execution. But they are also different because asynchronous calls usually involve a callback or an event, to signal that the response is available, while in the case of non-blocking the call returns with whatever is available and the caller might have to try again to get the rest of the data. The word non-blocking is usually used with I/O, while asynchronous is used in a more generic sense with a broader range of operations.

```
In particular, asynchronous I/O, in full generality, means I/O that is done independently of the current
flow of execution.
```

And here's a more specific example of asynchronous I/O:
 
```
Let's postulate that "synchronous I/O" means to issue an I/O command and then waiting until the I/O
command completes. Thus, if you issue a read command then the flow of execution will wait until something
is actually read. (Your thread will block until the read operation can be fulfilled.) Then asynchronous
I/O would mean "not synchronous I/O". That is, you issue an I/O command and it will be completed at some
later time. It follows that to do asynchronous I/O you need an interface that allows you to issue I/O
commands without blocking and to notify you when the I/O operation is completed.
```
     
```
An example I can think of is POSIX's select() interface. It should be relatively easy to write wrapper
code around it such that reads and writes from and to a number of file descriptors are queued and then
when those operations are completed events would be generated. What happens to those events can vary
depending on the interface's design goals. An obvious choice would be to call any callback functions
that the user of such an interface may have provided.
```

### Non-blocking operations

Non-blocking on the other hand is explained with this nice analogy in this StackOverflow answer:
 
```
This term is mostly used with IO. What this means is that when you make a system call, it will return
immediately with whatever result it has without putting your thread to sleep (with high probability).
For example non-blocking read/write calls return with whatever they can do and expect caller to execute
the call again. try_lock for example is non-blocking call. It will lock only if lock can be acquired.
Usual semantics for systems calls is blocking. read will wait until it has some data and put calling
thread to sleep.
```

 
Now that we've understood both the concepts in more detail, here's a neat real world example which clearly brings out the difference between the two:
 
```
For example in the classic sockets API, a non-blocking socket is one that simply returns immediately
with a special "would block" error message, wherever a blocking socket would have blocked. You have
to use a separate function such as select or poll to find out when is a good time to retry.
```

``` 
But asynchronous sockets (as supported by Windows sockets), or the asynchronous IO pattern used in
.NET, are more convenient. You call a method to start an operation, and the framework calls you back
when it's done. Even here, there are basic differences. Asynchronous Win32 sockets "marshal" their
results onto a specific GUI thread by passing Window messages, whereas .NET asynchronous IO is
free-threaded (you don't know what thread your callback will be called on).
```

# Short introduction to NIO. When? Where?

## Stream oriented vs Buffer oriented
The first big difference between Java NIO and IO is that IO is stream oriented, where NIO is buffer oriented. So, what does that mean?

Java IO being stream oriented means that you read one or more bytes at a time, from a stream. What you do with the read bytes is up to you. They are not cached anywhere. Furthermore, you cannot move forth and back in the data in a stream. If you need to move forth and back in the data read from a stream, you will need to cache it in a buffer first.

Java NIO's buffer oriented approach is slightly different. Data is read into a buffer from which it is later processed. You can move forth and back in the buffer as you need to. This gives you a bit more flexibility during processing. However, you also need to check if the buffer contains all the data you need in order to fully process it. And, you need to make sure that when reading more data into the buffer, you do not overwrite data in the buffer you have not yet processed.

## Blocking vs Non-blocking I/O
Java IO's various streams are blocking. That means, that when a thread invokes a read() or write(), that thread is blocked until there is some data to read, or the data is fully written. The thread can do nothing else in the meantime.

Java NIO's non-blocking mode enables a thread to request reading data from a channel, and only get what is currently available, or nothing at all, if no data is currently available. Rather than remain blocked until data becomes available for reading, the thread can go on with something else.

The same is true for non-blocking writing. A thread can request that some data be written to a channel, but not wait for it to be fully written. The thread can then go on and do something else in the mean time.

What threads spend their idle time on when not blocked in IO calls, is usually performing IO on other channels in the meantime. That is, a single thread can now manage multiple channels of input and output.

## Buffers
 Java NIO Buffers are used when interacting with NIO Channels. As you know, data is read from channels into buffers, and written from buffers into channels.

A buffer is essentially a block of memory into which you can write data, which you can then later read again. This memory block is wrapped in a NIO Buffer object, which provides a set of methods that makes it easier to work with the memory block. 

Basic Buffer Usage

Using a Buffer to read and write data typically follows this little 4-step process:

    Write data into the Buffer
    Call buffer.flip()
    Read data out of the Buffer
    Call buffer.clear() or buffer.compact()

When you write data into a buffer, the buffer keeps track of how much data you have written. Once you need to read the data, you need to switch the buffer from writing mode into reading mode using the flip() method call. In reading mode the buffer lets you read all the data written into the buffer.

Once you have read all the data, you need to clear the buffer, to make it ready for writing again. You can do this in two ways: By calling clear() or by calling compact(). The clear() method clears the whole buffer. The compact() method only clears the data which you have already read. Any unread data is moved to the beginning of the buffer, and data will now be written into the buffer after the unread data.

Here is a simple Buffer usage example, with the write, flip, read and clear operations maked in bold: 

## Channels
Typically, all IO in NIO starts with a Channel. A Channel is a bit like a stream. From the Channel data can be read into a Buffer. Data can also be written from a Buffer into a Channel. Here is an illustration of that:
There are several Channel and Buffer types. Here is a list of the primary Channel implementations in Java NIO:

FileChannel
DatagramChannel
SocketChannel
ServerSocketChannel
As you can see, these channels cover UDP + TCP network IO, and file IO.

There are a few interesting interfaces accompanying these classes too, but I'll keep them out of this Java NIO overview for simplicity's sake. They'll be explained where relevant, in other texts of this Java NIO tutorial.

Here is a list of the core Buffer implementations in Java NIO:

ByteBuffer
CharBuffer
DoubleBuffer
FloatBuffer
IntBuffer
LongBuffer
ShortBuffer
These Buffer's cover the basic data types that you can send via IO: byte, short, int, long, float, double and characters.

Java NIO also has a MappedByteBuffer which is used in conjunction with memory mapped files. I'll leave this Buffer out of this overview though.

## Selectors
A Selector is a Java NIO component which can examine one or more NIO Channel's, and determine which channels are ready for e.g. reading or writing. This way a single thread can manage multiple channels, and thus multiple network connections. 

A Selector allows a single thread to handle multiple Channel's. This is handy if your application has many connections (Channels) open, but only has low traffic on each connection. For instance, in a chat server.

To use a Selector you register the Channel's with it. Then you call it's select() method. This method will block until there is an event ready for one of the registered channels. Once the method returns, the thread can then process these events. Examples of events are incoming connection, data received etc. 

 The Channel must be in non-blocking mode to be used with a Selector. This means that you cannot use FileChannel's with a Selector since FileChannel's cannot be switched into non-blocking mode. Socket channels will work fine though.

Notice the second parameter of the register() method. This is an "interest set", meaning what events you are interested in listening for in the Channel, via the Selector. There are four different events you can listen for:

    Connect
    Accept
    Read
    Write

A channel that "fires an event" is also said to be "ready" for that event. So, a channel that has connected successfully to another server is "connect ready". A server socket channel which accepts an incoming connection is "accept" ready. A channel that has data ready to be read is "read" ready. A channel that is ready for you to write data to it, is "write" ready.

These four events are represented by the four SelectionKey constants:

    SelectionKey.OP_CONNECT
    SelectionKey.OP_ACCEPT
    SelectionKey.OP_READ
    SelectionKey.OP_WRITE

If you are interested in more than one event, OR the constants together, like this: 

# Reactor pattern and proactor pattern

# Implementation of NIO on JVM. What are selectors, channels and selectorkeys

# Speed comparison between NIO and SIO

- InfiniBand - Mellanox and Intel manufacture InifiBand host bus adapters and network switches. In February 2016 it was reported that Oracle Corporation had engineered its own InifiniBand switch units and server adapter chips.

# C10K problem and NIO fix for it

# Do not block - ever

- non-blocking JDBC API - Douglas Surber - Java One / September 2016



https://msdn.microsoft.com/pl-pl/library/windows/desktop/aa365683(v=vs.85).aspx
http://docs.oracle.com/cd/E19253-01/816-4854/character-11/index.html
https://en.wikipedia.org/wiki/Asynchronous_I/O#Channel_I.2FO
http://www.programmr.com/blogs/difference-between-asynchronous-and-non-blocking
https://en.wikipedia.org/wiki/Non-blocking_I/O_(Java)
http://www.ibm.com/developerworks/java/tutorials/j-nio/j-nio.html
http://tutorials.jenkov.com/java-nio/overview.html
http://adblogcat.com/asynchronous-java-nio-for-dummies/
http://www.javaworld.com/article/2078654/core-java/java-se-five-ways-to-maximize-java-nio-and-nio-2.html
http://rox-xmlrpc.sourceforge.net/niotut/
http://jeewanthad.blogspot.com/2013/02/reactor-pattern-explained-part-1.html
http://www.ibm.com/developerworks/library/j-jvmc3/
https://en.wikipedia.org/wiki/Input/output
https://en.wikipedia.org/wiki/Channel_I/O
http://stackoverflow.com/questions/15850033/what-are-the-benefits-of-java-nio-for-a-web-server

# Reactor/Proactor
http://www.puncsky.com/blog/2015/01/13/understanding-reactor-pattern-for-highly-scalable-i-o-bound-web-server/
http://parijatmishra.blogspot.com/2008/01/reactor-vs-proactor.html

# NIO
http://gee.cs.oswego.edu/dl/cpjslides/nio.pdf
https://dzone.com/articles/java-nio-vs-io
http://stackoverflow.com/questions/10372066/what-is-the-exact-use-of-java-nio-package-when-already-methods-are-available-wit
https://en.wikipedia.org/wiki/Non-blocking_I/O_(Java)
http://tutorials.jenkov.com/java-nio/nio-vs-io.html
http://stackoverflow.com/questions/25537675/java-what-exactly-is-the-difference-between-nio-and-nio-2
http://howtodoinjava.com/core-java/io/difference-between-standard-io-and-nio/

# NIO speed
http://www.mailinator.com/tymaPaulMultithreaded.pdf
http://mailinator.blogspot.in/2008/02/kill-myth-please-nio-is-not-faster-than.html
http://stackoverflow.com/questions/16058938/io-and-nio-performance-difference-and-example
http://stackoverflow.com/questions/7611152/nio-performance-improvement-compared-to-traditional-io-in-java
https://www.quora.com/Why-is-the-NIO-non-blocking-input-output-code-faster-than-that-of-Java-IO
https://www.quora.com/How-big-is-the-performance-difference-between-Java-IO-reading-writing-and-Java-NIO


# Tool to drawing
https://www.draw.io/