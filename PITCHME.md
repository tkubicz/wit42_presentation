# Java NIO

Let's talk about Java Non-blocking I/O

#HSLIDE

### Hello, it's nice to meet you!

* My name is Tymoteusz Kubicz.


#VSLIDE

### About me

* I work as a senior Java Developer at NetworkedAssets.
* Currently programming mostly in Scala. <!-- .element: class="fragment" -->


#VSLIDE

### Contact

- Twitter: @timkubicz
- Mail: tkubicz@me.com


#HSLIDE

### What is I/O?

IO is the communication between the information processing system and the outside world.
![Input Output](img/002_input_output.png)


#HSLIDE

### Synchronous and blocking operations

#VSLIDE

##### Http protocol is an example of synchronous I/O
![Http Protocol](img/001_http.png)


#VSLIDE

##### InputStream.read is an example of blocking I/O
```java
InputStream is = new FileInputStream("~/home/tku/file.txt");
int data = is.read();
while (data != -1) {
	// Let's do something with data
}
is.close();
```

#HSLIDE

### Asynchronous and Non-blocking operations

#VSLIDE

- Asynchronous and non-blocking operations are fairly simillar
- Asynchronous calls usually involves callback or event to indicate that the operation is done <!-- .element: class="fragment" -->
- Non-blocking call returns with whatever is currently available <!-- .element: class="fragment" -->
- The caller might try again to get the rest of the data <!-- .element: class="fragment" -->


#VSLIDE

##### The word non-blocking is usually used with I/O, while asynchronous is used in a more generic sense with a broader range of operations.

#HSLIDE

### Standard approach to I/O

Synchronous or blocking I/O


#VSLIDE

- Start the access and then wait for it to complete.
- This approach would block the progress of a program while the communication is in progress, leaving system resource idle. <!-- .element: class="fragment" -->
- With many I/O operations, the processor can spend almost all of its time idle waiting for I/O operations to complete. <!-- .element: class="fragment" -->


#HSLIDE

### Short history of Java NIO

#VSLIDE

- NIO was intoduced with JDK 1.4 (2002)
- NIO.2 was introduced with JDK 1.7 (2011) <!-- .element: class="fragment" -->


#VSLIDE

- Buffers for data of primitive types
- Channels, primitive I/O abstraction
- A new File interface
- Non-blocking I/O for writing servers
- AsynchronousChannels in Java 7

#VSLIDE

- High speed, memory block oriented I/O standard
- NIO takes adventage of low-level optimizations without using native code <!-- .element: class="fragment" -->

#HSLIDE

### Java NIO

#### Stream oriented vs Buffer oriented

- IO is stream oriented in Java <!-- .element: class="fragment" -->
- NIO is buffer oriented <!-- .element: class="fragment" -->


#VSLIDE

#### NIO Buffers

Typical usage:

* Write data into the buffer
* Call flip() method
* Read data out of the buffer
* Call clear() or compact() on buffer

#VSLIDE

#### Types of buffers

* ByteBuffer
* MappedByteBuffer (a special kind :))
* CharBuffer
* DoubleBuffer
* FloatBuffer
* IntBuffer
* LongBuffer
* ShortBuffer


#VSLIDE

##### The processing of data

```
Grabiszynska;Wroclaw;Poland
Schelleingasse;Vienna;Austria
Bahnhofstrasse;Zurich;Switzerland
...
```

```scala
val input = ... // Some input stream
val reader = new BufferedReader(new InputStreamReader(input))

val first = reader.readLine()
val second = reader.readLine()
...
```

![Blocking read](img/005_blocking_read.png)

#VSLIDE

```scala
val buffer = ByteBuffer.allocate(64)
var bytesRead = channel.read(buffer)

while(!isBufferFull(bytesRead)) {
	bytesRead = inChannel.read(buffer)
}
```

![Buffer read](img/006_buffer_read.png)

#VSLIDE

#### Blocking vs Non-blocking I/O

- Java I/O's various streams are blocking <!-- .element: class="fragment" -->
- NIO uses channels <!-- .element: class="fragment" -->

#VSLIDE

#### Channels

- All IO in NIO starts with a channel <!-- .element: class="fragment" -->
- Channel is simillar to "File Descriptor" in POSIX systems <!-- .element: class="fragment" -->
- Channel is like a stream <!-- .element: class="fragment" -->
- From channel data can be read into a buffer <!-- .element: class="fragment" -->
- And the other way - from the buffer into a channel <!-- .element: class="fragment" -->

![channel](img/003_channel_buffer.png)

#VSLIDE

#### Channels

- Primary channels implementations in Java NIO: FileChannel, DatagramChannel, SocketChannel, ServerSocketChannel
- These channels cover UDP + TCP network IO and file IO <!-- .element: class="fragment" -->

#VSLIDE

##### Selectors

- A selector allows a single thread to handle multiple channels <!-- .element: class="fragment" -->
- Very handy for many connections (Channels) <!-- .element: class="fragment" -->
- Less threading <!-- .element: class="fragment" -->

![selector](img/004_selector.png)


#VSLIDE

#### Registering channels
- To use Channel with a Selector is is necessary to register the the Channel with the Selector <!-- .element: class="fragment" -->
- The Channel must be in non-blocking mode <!-- .element: class="fragment" -->
- You cannot use FileChannel because it cannot be switched to non-blocking mode <!-- .element: class="fragment" -->
- Available events: Connect, Accept, Read, Write <!-- .element: class="fragment" -->

```java
channel.configureBlocking(false);
SelectionKey key = channel.register(selector, SelectionKey.OP_CONNECT);
```

#VSLIDE

#### Selecting Channels
- Using select(), select(long timeout), selectNow()
- These methods return the channels that are "ready" for the events you are interesed in

#VSLIDE

#### Selector full example

```scala
val channel = SocketChannel.open()
channel.configureBlocking(false)
channel.connect(new InetSocketAddress("http://google.com", 80))
val selector = Selector.open()
val key = channel.register(selector, SelectionKey.OP_READ)

while(true) {
  val readChannels = selector.select()
  if (readChannels != 0) {
    val selectedKeys = selector.selectedKeys()
    selectedKeys.asScala.foreach { k =>
      if (key.isAcceptable) {/* A connection was accepted */}
      else if (key.isConnectable) {/* A connection was established */}
      else if (key.isReadable) {/* A channel is ready for reading */}
      else if (key.isWritable) {/* A channel is ready for writing */}
    }
  }
}
```

#VSLIDE

### Summary

- NIO allows to manage multiple channels (network connections or files) using only single (or few) threads
- Parsing data is harder <!-- .element: class="fragment" -->


#HSLIDE

### C10K problem

- What is that? <!-- .element: class="fragment" -->
- The term was coined in 1999 by Dan Kegel (Simtel, cdrom.com) <!-- .element: class="fragment" -->
- The problem has since been used for the general issue of large number of clients <!-- .element: class="fragment" -->
- Most recently C10M in the 2010s <!-- .element: class="fragment" -->
- 2 million connections - WhatsApp, 24 cores, using Erlang on FreeBSD <!-- .element: class="fragment" -->
- 10-12 million connections - MigratoryData, 12 cores, using Java on Linux <!-- .element: class="fragment" -->


#HSLIDE

### Let's talk about servers

- Thread per connection
- NIO approach to server <!-- .element: class="fragment" -->

#VSLIDE

- NIO servers are better for thousands of simultanously open connections
- Better when there is a little data per connection

![Single Thread Multiple Connections](img/007_single_thread_multiple_connections.png)

#VSLIDE

- Classic approach might be better for a fewer connections with very high bandwidth
- When sending a lot of data at a time

![Thread per connection](img/008_thread_per_connection.png)

#VSLIDE

#### Synchronous I/O, single connection per thread model
- One thread per connection <!-- .element: class="fragment" -->
- Many threads (thousands) <!-- .element: class="fragment" -->
- Scaling is limited <!-- .element: class="fragment" -->
- In Java, a typical standard value for the thread stack size (-Xss) is 256kb. With 10k threads, you are automatically using about 2GB of memory <!-- .element: class="fragment" -->
- Context switching, synchronization, memory overhead, thread pool starvation <!-- .element: class="fragment" -->


#VSLIDE

#### NIO Server thread model
- In this example, two threads. <!-- .element: class="fragment" -->
- It is possible to use more processor threads. <!-- .element: class="fragment" -->

![Reactor pattern](img/009_reactor_pattern.png)


#HSLIDE

##### If you don't need to implement your server from scratch, use proven solutions
- Netty, Apache Mina, Grizzly - free, open source
- CoralReactor - paid, approximately 10 times faster than the standard JDK NIO classes


#HSLIDE

### Speed comparison

- I'm not going that way... sorry
- NIO is not automatically faster faster than plain IO <!-- .element: class="fragment" -->
- Synchronous I/O is fast (see Paul Tyma lecture) <!-- .element: class="fragment" -->
- NIO is used because it has better scalability <!-- .element: class="fragment" -->
- Some operations are potentially faster using NIO because you don't need one thread per connection <!-- .element: class="fragment" -->
- For a super fast IO it is possible to use Async IO (Java 7+) with InfinBand (communication standard) <!-- .element: class="fragment" -->


#HSLIDE

### Do not block! Ever...

#VSLIDE

A real-life story how one blocking operation caused a series of catastrophic events

![Meps arch](img/010_meps_arch.png)

#VSLIDE

- If it's possible, try using asynchronous or non-blocking operations.
- Use Futures and Promises <!-- .element: class="fragment" -->
- If you are a Java developer, learn CompletableFuture <!-- .element: class="fragment" -->
- Try using non-blocking drivers to your databases <!-- .element: class="fragment" -->

#HSLIDE

#### Thanks you for your attention!

 - Presentation powered by http://gitpitch.com/
 - Images drawed using http://draw.io/
 - Available to download: https://gitlab.com/tkubicz/wit42_presentation/